//
//  SigninViewController.swift
//  Tasker
//
//  Created by Maxim Tolstykh on 04/06/2017.
//  Copyright © 2017 Brain Crysis. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTF.delegate = self
        self.passTF.delegate = self
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        
        var signedIn = true
        
        if (self.emailTF.text?.isEmpty)! {
            shakeTextField(textField: emailTF, numberOfShakes:0, direction :1, maxShakes : 10)
            signedIn = false
        }
        if (self.passTF.text?.isEmpty)! {
            shakeTextField(textField: passTF, numberOfShakes:0, direction :1, maxShakes : 10)
            signedIn = false
        }
        
        // try to auth from backend
        if (signedIn) {
            self.prepare(for: UIStoryboardSegue.init(identifier: "task.list", source: self, destination: TaskListTableViewController()), sender: sender)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func shakeTextField (textField : UITextField, numberOfShakes : Int, direction: CGFloat, maxShakes : Int) {
        
        let interval : TimeInterval = 0.03
        
        UIView.animate(withDuration: interval, animations: { () -> Void in
            textField.transform = CGAffineTransform(translationX: 5 * direction, y: 0)
            
        }, completion: { (aBool :Bool) -> Void in
            
            if (numberOfShakes >= maxShakes) {
                textField.transform = CGAffineTransform.identity
                textField.becomeFirstResponder()
                return
            }
            
            self.shakeTextField(textField: textField, numberOfShakes: numberOfShakes + 1, direction: direction * -1, maxShakes: maxShakes)
            
        })
    }
}

